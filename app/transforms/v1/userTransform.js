const transform = require('../transform');

class userTransform extends transform{
    userInfo(item){
        return{
            "_id": item._id,
            "fullname": item.fullname
        }
    }
}

module.exports = new userTransform()