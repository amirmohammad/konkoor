const validator = require('./validator'); 
const { check, body, sanitizeBody, buildCheckFunction } = require('express-validator');

class authValidator extends validator{

    login(){
        return[
            check('email')
                .trim()
                .not().isEmpty()
                .withMessage('ایمیل خالی است')
                .isEmail()
                .withMessage('فرمت ایمیل صحیح نیست'),

            check('password')
                .trim()
                .not().isEmpty()
                .withMessage('پسورد خالی است')
        ]
    }

    register(){
        return[
            check('email')
                .trim()
                .not().isEmpty()
                .withMessage('ایمیل خالی است')
                .normalizeEmail()
                .isEmail()
                .withMessage('فرمت ایمیل صحیح نیست'),
            sanitizeBody('notifyOnReply').toBoolean(),

            check('firstName')
                .trim()
                .not().isEmpty()
                .withMessage('نام را وارد کنید')
                .not().isNumeric()
                .not().contains('1','2','3','4','5','6','7','8','9')
                .withMessage('فرمت نام صحیح نیست'),
            sanitizeBody('notifyOnReply').toBoolean(),

            check('lastName')
                .trim()
                .not().isEmpty()
                .withMessage('نام خانوادگی را وارد کنید')
                .not().isNumeric()
                .not().contains('1','2','3','4','5','6','7','8','9')
                .withMessage('فرمت نام خانوادگی صحیح نیست'),
            sanitizeBody('notifyOnReply').toBoolean(),

            check('password')
                .trim()
                .not().isEmpty()
                .withMessage('پسورد را وارد کنید'),
            sanitizeBody('notifyOnReply').toBoolean(),

            check('phoneNumber')
                .trim()
                .isLength({min: 11})
                .withMessage('طول شماره صحیح نیست')
                .isNumeric()
                .withMessage('فرمت شماره صحیح نیست'),

                sanitizeBody('notifyOnReply').toBoolean()
        ]
    }
}

module.exports = new authValidator()