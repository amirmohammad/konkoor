const controller = require('../controller');
const User = require('../../../models/user');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

class registerController extends controller {
    
    showRegistrationForm(req , res) {
        res.render('auth/register');
    }

    async register(req, res, next){

        if(! await this.validationData(req, res)) return

        let email = req.body.email.trim().toLowerCase();
        let firstName = req.body.firstName.trim().toLowerCase();
        let lastName = req.body.lastName.trim().toLowerCase();
        let password = req.body.password.trim().toLowerCase();
        let phoneNumber = req.body.phoneNumber.toLowerCase();
        
        let user = await User.findOne({email: email});
        let user01 = await User.findOne({phoneNumber, });

        if(user || user01){
            return res.status(405).json({
                methode: 'register',
                code : '405',
                status: 'error',
                message : 'registered before',
                data:{}
            })
        }

        let newUser = new User({
            email,
            firstName, 
            lastName,
            password,
            phoneNumber,
            token:email,
        });

        

        let savedUser = await newUser.save(); 
        if(savedUser){
            return res.status(200).json({
                status : "OK",
                message : "register successful",
                code : '200',
                methode : "register",
                data: {
                    email: email, 
                }
            })
        }
        else{
            return res.status(500).json({
                status : "Error",
                message : "register failed",
                code : '500',
                methode : "register",
                data: null,
            })
        }
    }
}

module.exports = new registerController();