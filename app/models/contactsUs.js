const mongoose = require('mongoose'); 
const Schema = mongoose.Schema; 

const contactUsSchema = new Schema({
    userId : {type: Schema.Types.ObjectId, ref:"User", required: true}, 
    message : {type: String, required: true}, 
    email: {type: String},
    phoneNumber: {type: String}, 
    version: {type: String, default: "1"}
}, {timestamps: true, toJSON:{virtuals: true}});

module.export = mongoose.model('ContactUs', contactUsSchema);