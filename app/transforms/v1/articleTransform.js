const transform = require('../transform')
const moment = require('moment-jalaali')

module.exports = new class articleTransform extends transform{

    articles(item){
        // console.log(item)
        // console.log(date)
        var arr = new Array();
        for (let i of item){
            let date = moment(i.createdAt).format('jYYYY/jM/jD')
            let obj = {
                "_id": i._id,
                "title": i.title,
                "text": i.text,
                "image": i.image,
                "category": i.category.name,
                "author": i.author.fullname,
                "createdAt": date
            }
            arr.push(obj)
        }
        return arr;
    }

    async article(item){
        let obj = {
            "_id": item._id,
            "title": item.title,
            "text": item.text,
            "image": item.image,
            "category": item.category.name,
            "author": item.author.fullname,
            // "createdAt": moment(i.createdAt).format('jYYYY/jM/jD')
        }
        return obj
    }

}

