const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');
var moment = require('moment-jalaali');
moment.loadPersian(); 

const tokenSchema = Schema({
    accesstoken : { type : String},
    isacitve : { type : Boolean},
    token_type:{type : String},
    expires_in : { type : String },
    refresh_token : { type : String },
    scope: { type : String },
    date: {type: String, default : moment().format('jYYYY/jMM/jDD HH:mm')},
    version: {type: String, default: "1"},
} , { timestamps : true , toJSON : {virtuals : true} });

tokenSchema.plugin(mongoosePaginate);
    
module.exports = mongoose.model('Token' , tokenSchema);