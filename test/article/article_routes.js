process.env.NODE_ENV = 'test';
const querystring = require('querystring'); 
const mongoose = require('mongoose');
const Mockgoose = require('mockgoose').Mockgoose;
const mockgoose = new Mockgoose(mongoose); 
const config = require('config'); 
const request = require('request');
const chai = require('chai');
const should = chai.should();
const expect = chai.expect();
const chaiHttp = require('chai-http');
const fs= require('fs')

// const server = require('../../app/routes/api/v1/admin');
const server = require('../../app/routes/api/v1/article')
const articleRoute = require('../../app/routes/api/v1/article');

const supertest = require('supertest');
let Article = require('../../app/models/article');

describe('article routes', () => {

    it('list all articles', (done) => {
        let article = {
            category: 'test category 01',
            page: 1,
            token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiNWUxYzY4ZGZiYWQxMGQzYTI1YzIwNjcyIiwiaWF0IjoxNTc5NTM3MDExLCJleHAiOjE1ODAxNDE4MTF9.rjwwLkw9s8NlEgUkKTW3jqK3XKb2RJ03sk53gkpqqDY'
        }

        chai.request('localhost:1998')
            .get('/api/v1/article/')
            .send(article)
            .end((err , res) => {
                // console.log(res.body)
                res.should.have.status(200)
                res.body.should.be.a('object');
                res.body.data.articles.should.be.a('array');
                done()
            })
    })

    it('show specific aritcle by id ', (done) => {

        let data = {
            id: '5e184b8bf962bd721d053e48'
        }

        chai.request('localhost:1998')
            .get('/api/v1/article/'+ data.id)
            .end( (err, res) => {
                // console.log(res)
                res.should.have.status(200)
                res.should.have.property('body')
                res.body.should.have.property('data'); 
                done()
            })
        
    })
    

})