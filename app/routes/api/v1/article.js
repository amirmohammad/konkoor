const express = require('express');
const router = express.Router();

// const articleController = require("app/http/controllers/articleController");
const articleController = require('../../../http/controllers/articleController')

// validators
// const articleValidator = require('app/http/validators/articleValidator')
const articleValidator = require('../../../http/validators/articleValidator')

// shows all articles
router.get("/", articleController.articles ) ;

router.get("/:id", articleValidator.article(),articleController.article );
router.post("/inCategory",articleValidator.getArticlesInCategory(), articleController.getArticlesInCategory);


module.exports = router 