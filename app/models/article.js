const mongoose = require('mongoose'); 
const Schema = mongoose.Schema;
// const paginate = require('mongoose-paginate');
const mongoosePaginate = require('mongoose-paginate-v2')

const articleSchema = Schema({
    author: {type: Schema.Types.ObjectId, ref: "User"},
    title: { type: String, required: true, unique: true },
    text: {type: String, required: true}, 
    image: {type: String},
    category: {type: Schema.Types.ObjectId, ref: "Category", required: true},
}, {timestamps: true, toJSON:{virtuals: true}});

// mongoose.plugin(paginate);
mongoose.plugin(mongoosePaginate);

module.exports = mongoose.model("Article", articleSchema); 