const mongoose = require('mongoose'); 
const User = require('../../app/models/user');
const should = require('chai').should();
process.env.NODE_ENV = 'test';
const config = require('config');
const Mockgoose = require('mockgoose').Mockgoose;
const mockgoose = new Mockgoose(mongoose);


describe("auth test database", () => {

    var user = {
        author : true,
        admin : true,
        email: "amir01@gmail.com",
        password: '$2a$10$azXSXCkAk08PbexjWUH28uEQUit60lzZQlXkTHWRq1TtrLmKGi/1G',
        phoneNumber: '091234567980',
        firstName:"amirmohammad",
        lastName: "karamzade", 

    }


    // before(() => {
    //     mongoose.Promise = global.Promise;
    //     mongoose.set('useUnifiedTopology', true);
    //     mongoose.set('useCreateIndex', true);
    //     mongoose.connect(config.DBHost , { useNewUrlParser : true });    
    // })

    before(function(done) {
        
        mockgoose.prepareStorage().then(function() {
            mongoose.Promise = global.Promise;
            mongoose.set('useUnifiedTopology', true);
            mongoose.set('useCreateIndex', true);
            mongoose.set('useNewUrlParser', true); 
            mongoose.connect(config.DBHost, function(err) {
                done(err);
            });
        });
    });

    // it('check connection' , (done) => {
    //     mongoose.connection
    //         .once('open' , () => done())
    //         .on('error' , (err) => {
    //             console.log(err);
    //         });
    // });

    it('checked connection', (done) => {
        if ( mockgoose.helper.isMocked() === true ) {
            done()
        }
        else{
            done(err); 
        }
    })

    it('register a user', async () => {
        
        let newUser = new User(user)
        newUser = await newUser.save();

        // check for standard format
        user.should.be.a('object')
        user.should.have.property('author');
        user.should.have.property('admin');
        user.should.have.property('firstName');
        user.should.have.property('lastName');
        user.should.have.property('password');
        user.should.have.property('phoneNumber');
        user.should.have.property('email');

    })

    it('find all users', async () => {
        let users = await User.find();
        users.should.be.a('array');
    })

    it('find one User ', async () => {
        let phoneNumber = user.phoneNumber;

        let users = await User.findOne({ phoneNumber , });

        users.firstName.should.be.eql(users.firstName)
        users.lastName.should.be.eql(users.lastName)
        users.phoneNumber.should.be.eql(users.phoneNumber)
        users.email.should.be.eql(users.email)
        users.admin.should.be.eql(users.admin)
        users.author.should.be.eql(users.author)
    })

    it('update user', async () => {
        let phoneNumber = user.phoneNumber;
        let updateUser = await User.findOne({ phoneNumber , });

        updateUser.set({firstName: "ali"});
        let updated = await updateUser.save(); 

        updated.should.be.a('object')
        updated.firstName.should.be.eql('ali');
    })

    it('delete all users', async () => {
        let deleted = await User.deleteMany({});
        deleted.should.have.property('ok')
        deleted.ok.should.be.deep.eql(1);
    })

    after('close db connection',() => {
        mongoose.connection.close();
    })

})