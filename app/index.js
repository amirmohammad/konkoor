const express = require('express');
const app = express();
const http = require('http');
const path = require('path');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const validator = require('express-validator');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const mongoose = require('mongoose');
const flash = require('connect-flash');
const passport = require('passport');
const helmet = require('helmet')

const config = require('config');

module.exports = class Application {
    constructor() {
        this.setupExpress();
        this.setMongoConnection();
        this.setConfig();
        this.setRouters();
    }

    setupExpress() {
        const server = http.createServer(app);
        server.listen(conf.port , () => console.log(`Listening on port ${conf.port}`));
    }

    setMongoConnection() {
        mongoose.Promise = global.Promise;
        mongoose.set('useNewUrlParser', true)
        mongoose.set('useUnifiedTopology', true)
        mongoose.set('useCreateIndex', true)
        mongoose.set('useFindAndModify', false)
        mongoose.connect('mongodb://mongo:27017/konkoor');
    }

    /**
     * Express Config
     */
    setConfig() {
        //passport 
        // require('app/passport/passport-jwt')
        // require('app/passport/passport-local')

        app.use(express.static('public'));
        app.use('/uploads',express.static('uploads'));
        app.enable('trust proxy');

        app.set('view engine', 'ejs');
        app.set('views' , path.resolve('./resource/views'));

        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended : true }));
        // app.use(validator());
        app.use(passport.initialize());
        app.use(passport.session()); 

        

        app.use(helmet());
        app.use(session({
            secret : 'mysecretkey',
            resave : true,
            saveUninitialized : true,
            store : new MongoStore({ mongooseConnection : mongoose.connection })
        }));
        app.use(cookieParser('mysecretkey'));
        app.use(flash());
    }

    setRouters() {
        app.use(require('app/routes/api'));
        app.use(require('app/routes/web'));        
    }
}