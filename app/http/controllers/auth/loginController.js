// const controller = require('app/http/controllers/controller');
const controller = require('../controller');
const bcrypt = require('bcryptjs');
// const User = require('app/models/user');
const User = require('../../../models/user');
const jwt = require('jsonwebtoken'); 

class loginController extends controller {
    
    showLoginForm(req , res) {
        res.render('auth/login');
    }

    async login(req, res, next){
        if(! await this.validationData(req, res)) return

        let email = req.body.email.trim().toLowerCase();
        let password = req.body.password.trim().toLowerCase();

        // check username
        let user = await User.findOne({email});
        // if username not exists
        if(!user){
            return res.status(404).json({
                data : null,
                status : "error",
                message : "user not found",
                code : '404',
                methode : "login",
            })
        }
        // check password
        console.log(user.password)
        let passwordCompareResult = await bcrypt.compare(password, user.password);
        // password is incorrect
        if (!passwordCompareResult){
            return res.status(404).json({
                status : "error",
                message : "password incorrect!",
                code : '404',
                methode : "login",
            })
        }
        // authenticate user 
        
        else{
            let token = await jwt.sign({
                data: user._id,
            }, process.env.SECRERT_KEY, {expiresIn: '7d'});
            return res.status(200).json({
                status : "OK",
                message : "login successful",
                code : '200',
                methode : "login",
                data:{
                    token,
                }
            })
        }

        
        return  res.json(user);
    
    }

    // not used :((
    async login01(req , res) {
        try {
            if(! await this.validationData(req , res)) return;
            passport.authenticate('local.login' , { session : false } , (err , user , message =null) => {
                if(message) return res.status(200).json({
                    methode: 'login',
                    code : '400',
                    status: 'error',
                    message:'اطلاعات وارد شده صحیح نمی باشد/معتبر نمی باشد',
                    data : null
                })
                if(err) return this.failed(err.message , res);
                if(!user) return this.failed('چنین کاربری وجود ندارد', res , 404)

                req.login(user , { session : false } ,async (err) => {
                    if(err) return this.failed(err.message , res);



                    // create token
                    const token = jwt.sign({ user_id : user._id } , process.env.SECRERT_KEY , {
                    });
                    console.log(token);
                    await user.updateOne({smstoken:null});
                    await user.save();

                    let phonenumber = req.body.phonenumber ;
                    let result = await User.findOne({phonenumber});

                    if(! result || !result.name){
                        return res.json({
                            methode: 'login',
                            data : {
                                token,
                            },
                            code : '200',
                            status : 'success',
                            message : null

                        })
                    }

                    // if user found, see user information
                    if(result){
                        return res.json({
                            methode: 'login',
                            data : {
                                token,
                                name : result.name,
                                family: result.family,
                                email : result.email,
                                image : result.image,
                                accountType: result.accountType
                            },
                            code : '200',
                            status : 'success',
                            message : null

                        })
                    }
                    //console.log('token',token);

                })

            })(req , res);
        } catch (error) {
            // return  res.json('error');
            return res.json({
                data : null,
                status : "error",
                message : "please try again",
                code : '555',
                methode : "login"
            })
        }
    }
}

module.exports = new loginController();