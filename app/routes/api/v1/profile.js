const express = require('express');
const router = express.Router();

// const userController = require("app/http/controllers/userController");
const userController = require('../../../http/controllers/userController')

router.get('/', (req, res, next) =>  res.json('profile main route'))
router.get("/userInfo/:id", userController.getUserInfo)

module.exports = router ;