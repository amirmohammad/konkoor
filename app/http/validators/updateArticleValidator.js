const validator = require('./validator');
const { check } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

class getRefreshTokenValidator extends validator {
    
    handle() {
        return [
            check('articleID')
                .not().isEmpty()
                .escape(),
                sanitizeBody('notifyOnReply').toBoolean(),     
                check('title')
                .not().isEmpty()
                .escape(),
                sanitizeBody('notifyOnReply').toBoolean(),    
                check('text')
                .not().isEmpty()
                .escape(),
                sanitizeBody('notifyOnReply').toBoolean(),       
        ]

    }
}

module.exports = new getRefreshTokenValidator();