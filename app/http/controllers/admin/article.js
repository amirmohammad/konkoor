// schemas
const Article = require('../../../models/article');
// const Category = require('app/models/category'); 
const Category = require('../../../models/category');

const controller = require('../controller'); 

class articleController extends controller{

    async createArticle(req, res, next){

        if(! await this.validationData(req, res)) return
        
        if(!req.file){
            return res.status(404).json({
                methode: 'createArticle',
                code : '404',
                status: 'error',
                message : 'you should upload image',
                data:{}
            })
        }
        let filePath = req.file.path;
        let title = (req.body.title).trim();
        let articleBody = (req.body.text).trim();

        const searchTitleResult = await Article.findOne({title: title});
        
        if(searchTitleResult){
            return res.status(405).json({
                methode: 'createArticle',
                code : '400',
                status: 'error',
                message : 'این عنوان قبلا وجود داشته ',
                data:{}
            })
        }

        let isCategoryCorrect = await Category.findOne({_id: req.body.category});
        if(!isCategoryCorrect){
            return res.status(404).json({
                methode: 'createArticle',
                code : '404',
                status: 'error',
                message : 'category not found :|',
                data:{}
            })
        }

        let createdArticle = new Article({
            title,
            text: articleBody,
            image: filePath,
            category: req.body.category,
            author: req.user._id
        })

        const createdArticleResult = await createdArticle.save()
        
        if(createdArticleResult){
            return res.status(200).json({
                methode: 'createArticle',
                code : '200',
                status: 'OK',
                message : 'با موفقیت ذخیره شد',
                data:{}
            })
        }
        else{
            return res.status(405).json({
                methode: 'createArticle',
                code : '405',
                status: 'failed',
                message : 'خطا در ذخیره',
                data:{}
            })
        }
    }

    async updateArticle(req, res, next){

        if(! await this.validationData(req, res)) return

        if(!req.file){
            return res.status(404).json({
                methode: 'updateArticle',
                code : '404',
                status: 'error',
                message : 'you should upload image',
                data:{}
            })
        }
        // let articleID = req.body.articleID;
        let id = req.body.id || req.params.id;
        let isIDCorrect = await Article.findOne({_id: id});
        if(!isIDCorrect){
            return res.status(404).json({
                methode: 'createArticle',
                code : '404',
                status: 'error',
                message : 'article not found :|',
                data:{}
            })
        }

        let isCategoryCorrect = await Category.findOne({_id: req.body.category});
        if(!isCategoryCorrect){
            return res.status(404).json({
                methode: 'createArticle',
                code : '404',
                status: 'error',
                message : 'category not found :|',
                data:{}
            })
        }

        let update = await Article.findOneAndUpdate({_id: id}, {title: req.body.title, text: req.body.text, image:req.file.path, category:req.body.category, author: req.user._id}) ;

        if(update){
            return res.status(200).json({
                methode: 'updateArticle',
                code : '200',
                status: 'OK',
                message : 'با موفقیت ذخیره شد',
                data:{}
            })
        }
        else{
            return res.status(500).json({
                methode: 'updateArticle',
                code : '500',
                status: 'failed',
                message : 'خطا در ذخیره',
                data:{}
            })
        }
    }

    async deleteArticle(req, res, next){
        let id = req.params.id;
        if(! await this.validationData(req, res)) return
        let deleteResult = await Article.findOneAndDelete({_id: id});
        if (deleteResult){
            return res.status(200).json({
                methode: 'deleteArticle',
                code : '200',
                status: 'OK',
                message : 'deleted',
                data:{}
            })
        }
        else{
            return res.status(405).json({
                methode: 'deleteArticle',
                code : '405',
                status: 'failed',
                message : 'error in deleting',
                data:{}
            })
        }
    }

    async test(req, res, next){
        if(! await this.validationData(req, res)){return}
        return  res.json('test');
    }
}

module.exports = new articleController();
