const express = require('express');
const router = express.Router();

// controllers
const articleController = require('../../../http/controllers/admin/article'); 
// const categoryController = require('app/http/controllers/categoryController');
const categoryController = require('../../../http/controllers/categoryController')

// middleware
// const {uploadImage} = require('app/http/middleware/uploadMiddleware')
const {uploadImage} = require('../../../http/middleware/uploadMiddleware')

// validators
const testValidator = require('../../../http/validators/testValidator')
const categoryValidator = require('../../../http/validators/categoryValidator');
const articleValidator = require('../../../http/validators/articleValidator');

router.get('/', (req, res, next) =>  res.json('admin router'))

// articles CRUD
router.post('/createArticle', uploadImage.single('image'), articleValidator.createArticle(), articleController.createArticle);
router.put('/updateArticle/:id', uploadImage.single('image'),articleValidator.updateArticle(), articleController.updateArticle);
router.delete('/deleteArticle/:id', articleValidator.deleteArticle(),articleController.deleteArticle);
// router.post('/test', testValidator.handle(), articleController.test);
router.post('/test', (req, res) => {
     res.json(123);
});

// category
// create category
router.post('/createCategory', categoryValidator.createCategory(), categoryController.createCategory );
router.put('/updateCategory', categoryValidator.updateCategory(), categoryController.updateCategory);
router.delete('/deleteCategory',categoryValidator.deleteCategory(), categoryController.deleteCategory);
// delete category


module.exports = router;
