const express = require('express');
const router = express.Router();
    
// const loginController = require('app/http/controllers/auth/loginController');
const loginController = require('../../../../http/controllers/auth/loginController');

router.get("/", (req, res, next) =>  res.json("login router"));
router.post("/",loginController.login);

module.exports = router;