const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const mongoosePaginate = require('mongoose-paginate');

const sms = Schema({
    senderid : { type : Schema.Types.ObjectId , required:true, index: true, ref:"User"} ,
    recivephonenumber : {type:Schema.Types.ObjectId , required:true},
    notifPhone:{type:String, required:true},
    message : {type : String, required:true},
    version: {type: String, default: "1"},

} , { timestamps : true , toJSON : {virtuals : true} });

sms.plugin(mongoosePaginate);

module.exports = mongoose.model('Sms' , sms);