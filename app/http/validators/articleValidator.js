const { check, body, sanitizeBody, buildCheckFunction } = require('express-validator'); 
const checkParams = buildCheckFunction(['params', 'body'])
const validator = require('./validator');

class articleValidator extends validator{

    getArticlesInCategory(){
        return[
            check('category')
                .trim()
                .not().isEmpty()
                .withMessage("کتگوری خالی است :|"),
                sanitizeBody('notifyOnReply').toBoolean(),
        ]
    }

    createArticle(){
        return[
            check('category')
                .trim()
                .not().isEmpty()
                .withMessage("کتگوری خالی است :|"),
            check('title')
                .trim()
                .not().isEmpty()
                .withMessage("عنوان خالی است :|"),
                sanitizeBody('notifyOnReply').toBoolean(),
            check('text')
                .trim()
                .not().isEmpty()
                .withMessage("متن خالی است :|")
                .isLength({min: 20})
                .withMessage('طول متن پیام بیش از حد کوتاه است'),
                sanitizeBody('notifyOnReply').toBoolean(),
        ]
    }

    updateArticle(){
        return[
            check('category')
                .trim()
                .not().isEmpty()
                .withMessage("کتگوری خالی است :|"),

            check('title')
                .trim()
                .not().isEmpty()
                .withMessage("عنوان خالی است :|"),

            check('text')
                .trim()
                .not().isEmpty()
                .withMessage("متن خالی است :|"),

            checkParams('id')
                .isMongoId()
                .withMessage('id اشتباه است')
                .not().isEmpty()
                .withMessage('id خالی است'),

            sanitizeBody('notifyOnReply').toBoolean(),
        ]
        
    }

    article(){
        return[
            checkParams('id')
                .isMongoId()
                .withMessage('id اشتباه است')
                .not().isEmpty()
                .withMessage('id خالی است'),
        ]
    }

    deleteArticle(){
        return[
            checkParams('id')
                .isMongoId()
                .withMessage('id اشتباه است')
                .not().isEmpty()
                .withMessage('id خالی است'),
        ]
    }
    // deleteArticle(){}
}

module.exports = new articleValidator();