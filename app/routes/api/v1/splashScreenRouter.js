const express = require('express');
const router = express.Router();

// controllers
// const splashScreenController = require('app/http/controllers/splashScreenController');
const splashScreenController = require('../../../http/controllers/splashScreenController');
router.post("/", (req, res, next) =>  res.json("splashScreenMainrouter"))
router.get('/getWellcomeMessage', splashScreenController.wellcomeMessage )

module.exports = router; 