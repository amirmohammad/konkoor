// import transform from '../transform';
const transform = require('../transform');

class categoryTransform extends transform{
    async categories(item){
        let arr = new Array();

        for (let i of item){
            let obj = {
                "_id" :i._id,
                "name": i.name
            }
            arr.push(obj)
        }
        return arr
    }
}

module.exports = new categoryTransform()