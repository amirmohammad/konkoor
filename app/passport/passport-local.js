const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const User = require('app/models/user');


passport.serializeUser(function(user, done) {
    done(null, user.id);
});
   
passport.deserializeUser(function(id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});




passport.use('local.login' , new localStrategy({
    usernameField : 'phoneNumber',
    passwordField : 'token',
    passReqToCallback : true
} , (req , usernameField ,  passwordField , done) => {
    User.findOne({$and : [{phonenumber :usernameField }, {smstoken:passwordField}]} , (err , user) => {
        if(err) return done(err);
        if(user && user.smstoken && user.smstoken === passwordField){
            done(null , user);
        }else{
            done(null, false, 'اطلاعات وارد شده صحیح نمیباشد');
        }

        
    })
}))