// ...rest of the initial code omitted for simplicity.
const { check, body, sanitizeBody } = require('express-validator'); 
const validator = require('./validator');

class testValidator extends validator{
    handle(){
        return [
            check('username')
                .trim()
                .not().isEmpty()
                .withMessage("khalie")
                .isEmail()
                .normalizeEmail()
                .withMessage("shit"),
            check('password').isLength({min: 5}),
            sanitizeBody('notifyOnReply').toBoolean()
        ]
    }
}

module.exports = new testValidator();