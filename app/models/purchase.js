const mongoose = require('mongoose'); 
const Schema = mongoose.Schema; 

const purchaseSchema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: "User"},
    productId: {type: Schema.Types.ObjectId, ref: "Product"},
    purchaseStartDate: {type: String, required: true},
    purchaseEndDate: {type: String, required: true}, 
    version: {type: String, default: "1"},
    price: {type: Number, required: true}, 
    
}, {timestamps: true, toJSON:{virtuals: true}});


module.export = mongoose.model('Purchase', purchaseSchema)