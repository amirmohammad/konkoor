const controller = require('./controller');
// const Category = require('app/models/category')
const Category = require('../../models/category'); 
const Article = require('../../models/article');

// const categoryTransform = require('app/transforms/v1/categoryTransform');
const categoryTransform = require('../../transforms/v1/categoryTransform');

class categoryController extends controller{

    async createCategory(req, res, next){
        if(! await this.validationData(req, res)){
            return
        }
        let {category} = req.body;
        
        let result = await Category.findOne({name: category});

        if (result != null){
            return res.status(405).json({
                status : "Error",
                message : "category exists",
                code : '405',
                methode : "createCategory",
                data: category,
            })
        }
        let newCategory = new Category({
            name: category, 
        })

        let saved = await newCategory.save();
        if(saved){
            return res.json({
                status : "OK",
                message : "category saved",
                code : '200',
                methode : "createCategory",
                data: category,
            })
        }
        else{
            return res.json({
                status : "Error",
                message : "category not saved (server error)",
                code : '500',
                methode : "createCategory",
                data: category,
            })
        }
    }

    async updateCategory(req, res, next){
        if(! await this.validationData(req, res)){
            return
        }
        let {oldCategory, newCategory} = req.body; 

        let result = await Category.findOne({name: oldCategory});

        if(!result){
            return res.status(406).json({
                status : "Error",
                message : "old name is not valid",
                code : '406',
                methode : "updateCategory",
                data: {
                    "oldCategory": oldCategory, 
                    "newCategory": newCategory,
                },
            })
        }

        let updateResult = await Category.updateOne({name: oldCategory.trim()}, {name: newCategory.trim()});

        if(!updateResult){
            return res.status(500).json({
                status : "Error",
                message : "can not save update (server error)",
                code : '500',
                methode : "updateCategory",
                data: {},
            })
        }

        return res.status(200).json({
            status : "OK",
            message : "successful",
            code : '200',
            methode : "updateCategory",
            data: {
                oldCategory,
                newCategory,
            },
        })
    }

    async deleteCategory(req, res, next){
        if(! await this.validationData(req, res)){
            return
        }
        let categoryId = await Category.findOne({name: req.body.name},'id');
        if (!categoryId){
            return res.status(404).json({
                status : "error",
                message : "category not found",
                code : '404',
                methode : "deleteCategory",
                data: {},
            })
        }
        
        let result = await Category.findOneAndDelete({name: req.body.name}) ;

        let DeleteArticle = await Article.deleteMany({category: categoryId._id})
        
        if(result){
            return res.status(200).json({
                status : "OK",
                message : "category deleted",
                code : '200',
                methode : "deleteCategory",
                data: {
                    articlesDeleted: DeleteArticle.deletedCount
                },
            })
        }
        else{
            return res.status(400).json({
                status : "Error",
                message : "error in deleting category",
                code : '400',
                methode : "deleteCategory",
                data: {},
            })
        }
    }

    async categories(req, res, next){
        let categories = await Category.find({});

        return res.json({
            status : "OK",
            message : "successful",
            code : '200',
            methode : "categories",
            // data:{
            //     ...categories
            // },
            data:{
                categories: await categoryTransform.categories(categories),
            }
        })
    }
}


module.exports = new categoryController();