const express = require('express');
const router = express.Router();

const registerController = require('../../../../http/controllers/auth/registerController')

// packages
const passport = require('passport'); 

router.get("/", (req, res, next) =>  res.json("register router"));
router.post('/', registerController.register );


module.exports = router;