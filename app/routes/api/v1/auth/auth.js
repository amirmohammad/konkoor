const express = require('express');
const router = express.Router();

// login, register routers : 
const loginRouter = require('./login');
const registerRouter = require('./register'); 

// const authValidator = require('app/http/validators/authValidator')
const authValidator = require('../../../../http/validators/authValidator');

router.get("/", (req, res, next) =>  res.json("auth main route"));

router.use("/login",authValidator.login(), loginRouter);
router.use("/register",authValidator.register(),     registerRouter); 

module.exports = router; 