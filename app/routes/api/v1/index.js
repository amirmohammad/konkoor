const express = require('express');
const router = express.Router();

const splashScreenRouter = require("./splashScreenRouter");
const settingsRouter = require("./settingsRouter"); 
const mainPageRouter = require("./mainPage");
const profileRouter = require("./profile"); 
const categoryRouter = require("./category")
const article = require("./article");
const admin = require("./admin"); 
const auth = require('./auth/auth');


// middleware 
// const apiAuthMiddleware = require('app/http/middleware/apiAuth')
const apiAuthMiddleware = require('../../../http/middleware/apiAuth')
// const adminMiddleware = require('app/http/middleware/adminAuth')
const adminMiddleware = require('../../../http/middleware/adminAuth')

// main api route
router.get('/',(req, res, next) => res.json('haji api version e 1 ham oke ')); 

// router middlewares
router.use('/splashScreen', splashScreenRouter); 
router.use('/settings', settingsRouter); 
router.use('/mainPage', mainPageRouter); 
router.use('/profile', apiAuthMiddleware ,profileRouter); 
router.use('/category', categoryRouter); 
router.use("/article", article);
router.use("/admin", apiAuthMiddleware, adminMiddleware.handle, admin); 
router.use('/auth', auth); 



module.exports = router;