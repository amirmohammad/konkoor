const mongoose = require('mongoose'); 
const Schema = mongoose.Schema; 

const faqSchema = new Schema({
    question: {type: String, required: true},
    answer: {type: String, required: true},
    version: {type: String, default: "1"}
}, {timestamps: true})

module.export = mongoose.model('FAQ', faqSchema);