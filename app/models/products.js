const mongoose = require('mongoose'); 
const Schema = mongoose.Schema; 

const productSchema = new Schema({
    title: {type: String, required: true}, 
    productType: {type: String, required: true}, 
    description: {type: String, required: true}, 
    price: {type: Number, required: true}, 
    discount: {type: Number}, 
    sku: {type: String, required:true, unique: true, },
    isActive: {type: Boolean, default: true},
    isSpecialPlan: {type: Boolean, default: false},
    version: {type: String, default: "1"}
}, {timestamps: true, toJSON:{virtuals: true}});


module.export = mongoose.model('Product', productSchema)