process.env.NODE_ENV = 'test';
const querystring = require('querystring'); 
const mongoose = require('mongoose');
const Mockgoose = require('mockgoose').Mockgoose;
const mockgoose = new Mockgoose(mongoose); 
const config = require('config'); 
const request = require('request');
const chai = require('chai');
const should = chai.should();
const expect = chai.expect();
const chaiHttp = require('chai-http');
const express = require('express');
const app = express();

const server = require('../../app/routes/api/index');
const articleRoute = require('../../app/routes/api/v1/article');

const supertest = require('supertest');

let Category = require('../../app/models/category');

chai.use(chaiHttp);


describe('category routes', () => {

    let mockCategory = {
        name : "آموزشی"
    }

    before(function(done) {

        if (!global.Promise) {
            global.Promise = require('q');
        }
        
        mockgoose.prepareStorage().then(function() {
            mongoose.Promise = global.Promise;
            mongoose.set('useUnifiedTopology', true);
            mongoose.set('useCreateIndex', true);
            mongoose.set('useNewUrlParser', true); 
            mongoose.connect(config.DBHost, function(err) {
                done(err);
            });
        });
    });

    it('list all categories', (done) => {
        let category = {
            category: 'test category 01',
            token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiNWUxYzY4ZGZiYWQxMGQzYTI1YzIwNjcyIiwiaWF0IjoxNTc5NTM3MDExLCJleHAiOjE1ODAxNDE4MTF9.rjwwLkw9s8NlEgUkKTW3jqK3XKb2RJ03sk53gkpqqDY'
        }

        chai.request('http://localhost:1998')
            .get('/api/v1/category/')
            .end((err , res) => {
                res.should.have.status(200)
                res.body.should.be.a('object');
                res.body.should.have.property('data')
                res.body.data.categories.should.be.a('array')
                done()
            })
    })

    it('list articles in specific catgery', (done) => {
        let data = {
            category : '5e20d7b857f89262fb522b2e',
            page : 1
        }

        chai.request('http://localhost:1998')
            .post('/api/v1/article/inCategory')
            // .set('content-type', 'application/json')
            .send(data)
            .end((err, res) => {
                if(err){
                    done(err)
                }
                res.body.should.be.a('object');
                res.body.should.have.property('data')
                done()
            })
    })

    it('create category', (done) => {

        let data ={
            token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiNWUxOWJlNGMwNjhlZmMxYmQ0ZGViOTg1IiwiaWF0IjoxNTc5NTY1ODA0LCJleHAiOjE1ODAxNzA2MDR9.Cfmfja6v7rqrunPVmg9Mapp22MklI_5MzYwDy8ROsj4' ,
            category: 'یشسب'
        }

        chai.request('localhost:1998')
            .post('/api/v1/admin/createCategory')
            .send(data)
            .end((err, res) => {
                // console.log(res)
                res.should.have.status(200)
                res.body.should.be.a('object');
                // res.body.data.should.have.property('data')
                res.body.data.should.be.eql(data.category)
                done()
            })
    })

    it('update category', (done) => {
        let data ={
            token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiNWUxOWJlNGMwNjhlZmMxYmQ0ZGViOTg1IiwiaWF0IjoxNTc5NTY1ODA0LCJleHAiOjE1ODAxNzA2MDR9.Cfmfja6v7rqrunPVmg9Mapp22MklI_5MzYwDy8ROsj4' ,
            oldCategory: 'یشسب',
            newCategory: 'فنی'
        }

        chai.request('localhost:1998')
            .put('/api/v1/admin/updateCategory')
            .send(data)
            .end((err, res) => {
                // console.log(res)
                res.should.have.status(200)
                res.body.should.be.a('object');
                // res.body.data.should.have.property('data')
                res.body.data.oldCategory.should.be.eql(data.oldCategory)
                done()
            })
    })

    it('delete category', (done) => {
        let data = {
            token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjoiNWUxOWJlNGMwNjhlZmMxYmQ0ZGViOTg1IiwiaWF0IjoxNTc5NTY1ODA0LCJleHAiOjE1ODAxNzA2MDR9.Cfmfja6v7rqrunPVmg9Mapp22MklI_5MzYwDy8ROsj4'  ,
            name: 'فنی',
        }
        chai.request('localhost:1998')
            .delete('/api/v1/admin/deleteCategory')
            .send(data)
            .end( (err, res) => {
                // console.log(res)
                res.body.should.be.a('object');
                res.should.have.status(200)
                done(); 
            })
    })

    after('close db connection',() => {
        mongoose.connection.close();
    })


})
