const express = require('express');
const router = express.Router();

// const categoryController = require('app/http/controllers/categoryController');
const categoryController = require('../../../http/controllers/categoryController'); 

router.get('/', categoryController.categories);

module.exports = router; 