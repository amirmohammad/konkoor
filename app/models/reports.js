const mongoose = require('mongoose'); 
const Schema = mongoose.Schema; 

const ReportSchema = new Schema({
    user: {type: Schema.Types.ObjectId, ref: "User"},
    title: {type: String},
    text: {type: String, required: true}, 
    version: {type: String, default: "1"},
    
}, {timestamps: true, toJSON:{virtuals: true}});


module.export = mongoose.model('Report', ReportSchema)