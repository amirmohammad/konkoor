const mongoose = require('mongoose'); 
const Schema = mongoose.Schema;
const uniqueString = require('unique-string'); 
const bcrypt = require('bcryptjs')


const userSchema = new Schema({
    firstName: {type: String, maxlength: 120},
    lastName:  {type: String, maxlength: 120},
    email: {type: String, required: true}, 
    phoneNumber: {type: String, required: true, unique: true},
    password: {type: String, required: true}, 
    author: {type: Boolean, default: false},
    admin: {type: String, default: false},
},{timestamps: true, toJSON:{ virtuals: true }});

userSchema.methods.hashPassword = function(password) {
    let salt = bcrypt.genSaltSync(15);
    let hash = bcrypt.hashSync(password , salt);
    return hash;
}

userSchema.methods.comparePassword = function(password) {
    return bcrypt.compareSync(password , this.password);
}


let fullnameVirtual = userSchema.virtual('fullname')
fullnameVirtual.get(function(){
    return this.firstName + " " + this.lastName;
})

userSchema.pre('save', function(next){
    bcrypt.hash(this.password, 15, (err, hash) => {
        this.password = hash;
        next();
    });
})


module.exports = mongoose.model("User", userSchema)