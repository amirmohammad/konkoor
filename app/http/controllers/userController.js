// const controller = require('app/http/controllers/controller');
const controller = require('./controller')
// const User = require('app/models/user')
const User = require('../../models/user');

// const userTransform = require('app/transforms/v1/userTransform')
const userTransform = require('../../transforms/v1/userTransform')

class userController extends controller{

    async getUserInfo(req, res, next){
        let result = await User.findOne({_id: req.user.id}, 'firstName lastName fullname');
        // return  res.json(req.user);
        result = userTransform.userInfo(result)
        return res.status(200).json({
            methode: 'getUserInfo',
            code : '200',
            status: 'OK',
            message : 'user found',
            data: await userTransform.userInfo(result)
        })
    }
}

module.exports = new userController();