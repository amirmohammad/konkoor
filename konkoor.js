
//global packages
require('app-module-path').addPath(__dirname); //unit test error
require('dotenv').config();

//set up variables
global.conf = require('./config/mainConfig');

const App = require('./app');
new App();