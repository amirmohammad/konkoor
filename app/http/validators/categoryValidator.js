const { check, body, sanitizeBody } = require('express-validator'); 
const validator = require('./validator');

class testValidator extends validator{
    createCategory(){
        return [
            check('category')
                .trim()
                .not().isEmpty()
                .withMessage('کتگوری نباید خالی باشد'),
                sanitizeBody('notifyOnReply').toBoolean(),
        ]
    }

    updateCategory(){
        return[
            check('oldCategory')
                .trim()
                .not().isEmpty()
                .withMessage("کتگوری قدیمی را وارد کنید"),
                sanitizeBody('notifyOnReply').toBoolean(),
            check('newCategory')
                .trim()
                .not().isEmpty()
                .withMessage("کتگوری جدید را وارد کنید"),
                sanitizeBody('notifyOnReply').toBoolean(),
        ]
    }

    deleteCategory(){
        return[
            check('name')
                .trim()
                .not().isEmpty()
                .withMessage("نام کتگوری را وارد کنید"),
                sanitizeBody('notifyOnReply').toBoolean(),
        ]
    }
}

module.exports = new testValidator();