const mongoose = require('mongoose'); 
const Category = require('../../app/models/category');
const should = require('chai').should();
process.env.NODE_ENV = 'test';
const config = require('config');
const Mockgoose = require('mockgoose').Mockgoose;
const mockgoose = new Mockgoose(mongoose);


describe("category test database", () => {

    var category = {
        name: "test category", 
    }


    // before(() => {
    //     mongoose.Promise = global.Promise;
    //     mongoose.set('useUnifiedTopology', true);
    //     mongoose.set('useCreateIndex', true);
    //     mongoose.connect(config.DBHost , { useNewUrlParser : true });    
    // })

    before(function(done) {
        
        mockgoose.prepareStorage().then(function() {
            mongoose.Promise = global.Promise;
            mongoose.set('useUnifiedTopology', true);
            mongoose.set('useCreateIndex', true);
            mongoose.set('useNewUrlParser', true); 
            mongoose.connect(config.DBHost, function(err) {
                done(err);
            });
        });
    });

    // it('check connection' , (done) => {
    //     mongoose.connection
    //         .once('open' , () => done())
    //         .on('error' , (err) => {
    //             console.log(err);
    //         });
    // });

    it('checked connection', (done) => {
        if ( mockgoose.helper.isMocked() === true ) {
            done()
        }
        else{
            done(err); 
        }
    })

    it('insert category', async () => {
        
        let newCategory = new Category(category)
        newCategory = await newCategory.save();

        // check for standard format
        newCategory.should.be.a('object')
        newCategory.should.have.property('name');

        // done()
        

    })

    it('find all categories', async () => {
        let categories = await Category.find();
        categories.should.be.a('array');
    })

    it('find one category ', async () => {
        let name = category.name;

        let categories = await Category.findOne({ name , });
        categories.name.should.be.eql(category.name)
    })

    it('update category', async () => {
        let name = category.name;
        let oldCategory = await Category.findOne({ name , });

        oldCategory.set({name: "تست"});
        let updated = await oldCategory.save(); 

        updated.should.be.a('object')
        updated.name.should.be.eql('تست');
    })

    it('delete all categories', async () => {
        let deleted = await Category.deleteMany({});
        deleted.should.have.property('ok')
        deleted.ok.should.be.deep.eql(1);
    })

    after('close db connection',() => {
        mongoose.connection.close();
    })

})