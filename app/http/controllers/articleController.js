const controller = require('./controller')

// const Article = require('app/models/article')
const Article = require('../../models/article');
const Category = require('../../models/category');

// const articleTransform = require('app/transforms/v1/articleTransform');
const articleTransform = require('../../transforms/v1/articleTransform')

class articleController extends controller{

    async article(req, res, next){

        if(! await this.validationData(req, res)) return; 

        let isArticleExists = await Article.findOne({_id: req.params.id});
        if(!isArticleExists){
            return res.status(404).json({
                methode: 'article',
                code : '404',
                status: 'error',
                message : 'article not found',
                data:{}
            })
        }

        let article = await Article.paginate({_id: req.params.id},{
            populate:[{ path: 'author' , select: 'firstName lastName fullname'},
                      { path: 'category', select:'name'}],
            });

        return res.status(200).json({
            methode: 'article',
            code : '200',
            status: 'OK',
            message : 'article found',
            data:{
                ... await articleTransform.article(article.docs[0]), 
            }
        })

    }

    async articles(req, res, next){
        let page = req.body.page || 1; 
        let articles = await Article.paginate({},{
            populate:[{ path: 'author' , select: 'firstName lastName fullname'},
                      { path: 'category', select:'name'}],
            page: page , 
            limit : 10});
        
        

        return res.status(200).json({
            methode: 'article',
            code : '200',
            status: 'OK',
            message : 'article found',
            data:{
                articles: await articleTransform.articles(articles.docs),
                totalDocs: articles.totalDocs,
                limit: articles.limit,
                totalPages: articles.totalPages,
                page: articles.page,
                pagingCounter: articles.pagingCounter,
                hasPrevPage: articles.hasPrevPage,
                hasNextPage: articles.hasNextPage,
                prevPage: articles.prevPage,
                nextPage: articles.nextPage,
            }
        })
    }

    async getArticlesInCategory(req, res, next){
        let category = req.body.category;
        let page = req.body.page || 1 ;
        let IsCategoryValid = await Category.find({_id: category})
        if(! IsCategoryValid){
            return res.json({
                methode: 'getArticlesInCategory',
                code : '404',
                status: 'error',
                message : 'category not found',
                data:{}
            })
        }

        let result = await Article.paginate({category}, {
            sort:{_id: 1},
            populate:[{ path: 'author' , select: 'firstName lastName fullname'},
                      { path: 'category', select:'name'}
                    ],
            page, 
            limit: process.env.RESULT_PAGE_LIMIT
        })

        return res.json({
            methode: 'article',
            code : '200',
            status: 'OK',
            message : 'article found',
            data:{
                
                articles: await articleTransform.articles(result.docs),
                totalDocs: result.totalDocs,
                limit: result.limit,
                totalPages: result.totalPages,
                page: result.page,
                pagingCounter: result.pagingCounter,
                hasPrevPage: result.hasPrevPage,
                hasNextPage: result.hasNextPage,
                prevPage: result.prevPage,
                nextPage: result.nextPage,

            }
        })
        
    }


    

    
}

module.exports = new articleController();