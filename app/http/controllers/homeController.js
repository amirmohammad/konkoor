const controller = require('app/http/controllers/controller');

class homeController extends controller {
    
    index(req , res) {
        res.render('home');
    }

    message(req, res) {
        return res.render('index.ejs');
    }
}

module.exports = new homeController();