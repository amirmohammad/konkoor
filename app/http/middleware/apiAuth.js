const jwt = require('jsonwebtoken');
// const User = require(`${config.path.model}/user`);
// const User = require('app/models/user')
const User = require('../../models/user');

module.exports = (req , res , next) =>  {
    let token = req.body.token || req.query.token || req.headers['x-access-token'];

    if(token) {
        return jwt.verify(token , process.env.SECRERT_KEY , (err , decode ) => {
            if(err) {
                return res.json({
                    success : false ,
                    data : 'Failed to authenticate token.'
                })
            } 
            
            User.findById(decode.data , (err , user) => {
                if(err) throw err;

                if(user) {
                    user.token = token;
                    req.user = user;
                    next();
                } else {
                    return res.json({
                        success : false ,
                        data : 'User not found'
                    });
                }
            }) 

            // next();
            // return;
        })
    }

    return res.status(403).json({
        data : 'No Token Provided',
        success : false
    })
}