const autoBind = require('auto-bind');
const { validationResult } = require('express-validator')

module.exports = class controller {
    constructor() {
        autoBind(this);
    }

    async validationData(req , res) {
        const result = validationResult(req);
        if (! result.isEmpty()) {
            const errors = result.array();
            const messages = [];
           
            errors.forEach(err => messages.push(err.msg));

            this.failed(messages , res , 403);

            return false;
        }

        return true;
    }

    async havename(user, res){
        if(!user.name){
            return res.json({
                status: "error",
                message : "423"
            })
        }
        else return
    }

    async isMongoId(paramId, res) {
        if(! isMongoId(paramId)) return false
        return true
    }

    
    async escapeAndTrim(feild) {
        let item = feild.trim();
        item = escape(item);
        return item
    }

    failed(msg , res , statusCode = 500) {
        return res.json({
            code: '433',
            message : msg,
            status : 'error'
        })
    }
}