const mongoose = require('mongoose'); 
const Article = require('../../app/models/article');
const should = require('chai').should();
process.env.NODE_ENV = 'test';
const config = require('config');
const Mockgoose = require('mockgoose').Mockgoose;
const mockgoose = new Mockgoose(mongoose);


describe("article test database", () => {

    var article = {
        author : '5e25a092f6079357fd714b89',
        title : 'title 01', 
        text: "123456798adsfadf",
        image: 'image/test.jpg',
        category: '5e25a092f6079357fd714b89',

    }


    // before(() => {
    //     mongoose.Promise = global.Promise;
    //     mongoose.set('useUnifiedTopology', true);
    //     mongoose.set('useCreateIndex', true);
    //     mongoose.connect(config.DBHost , { useNewUrlParser : true });    
    // })

    before(function(done) {
        
        mockgoose.prepareStorage().then(function() {
            mongoose.Promise = global.Promise;
            mongoose.set('useUnifiedTopology', true);
            mongoose.set('useCreateIndex', true);
            mongoose.set('useNewUrlParser', true); 
            mongoose.connect(config.DBHost, function(err) {
                done(err);
            });
        });
    });

    // it('check connection' , (done) => {
    //     mongoose.connection
    //         .once('open' , () => done())
    //         .on('error' , (err) => {
    //             console.log(err);
    //         });
    // });

    it('checked connection', (done) => {
        if ( mockgoose.helper.isMocked() === true ) {
            done()
        }
        else{
            done(err); 
        }
    })

    it('save Article', async () => {

        let newArticle = new Article(article)
        newArticle = await newArticle.save()
        

        // check for standard format
        newArticle.should.be.a('object')
        newArticle.should.have.property('author');
        newArticle.should.have.property('title');
        newArticle.should.have.property('text');
        newArticle.should.have.property('image');
        newArticle.should.have.property('category');

    })

    it('find all articles', async () => {
        let articles = await Article.find();
        articles.should.be.a('array');
    })

    it('find one article ', async () => {
        let title = article.title;

        let articles = await Article.findOne({ title , });

        articles.title.should.be.eql(article.title)
        articles.text.should.be.eql(article.text)
        articles.image.should.be.eql(article.image)
    })

    it('update article', async () => {
        let title = article.title;
        let updateCategory = await Article.findOne({ title , });

        updateCategory.set({title: "ali"});
        let update = await updateCategory.save(); 

        update.should.be.a('object')
        update.title.should.be.eql('ali');
    })

    it('delete all categories', async () => {
        let deleted = await Article.deleteMany({});
        deleted.should.have.property('ok')
        deleted.ok.should.be.deep.eql(1);
    })

    after('close db connection',() => {
        mongoose.connection.close();
    })

})